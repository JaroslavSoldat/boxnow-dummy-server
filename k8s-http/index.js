const express = require('express');
const {version} = require('./package.json');
console.log(`HTTP server v${version}`);

const id = (Math.random() + 1).toString(36).substring(2, 7);
const host = process.env.HOSTNAME || '? (pls set HOSTNAME)';

const server = express();

server.get('/', (req, res) => {
    const time = new Date();

    res.send(`
      <h1>K8S testing HTTP server v${version}</h1>
      <h1>${id} @ ${host}</h1>
      <h1>${time}</h1>
    `);
});

server.get('/error', () => process.exit(1));

server.listen(8080, () => console.log('Listening on port 8080'));

process.on('SIGTERM', () => process.exit(0));

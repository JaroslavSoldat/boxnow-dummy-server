const axios = require('axios')
const express = require('express');

const app = express();
const port = process.env.BOXNOW_PORT || 8080;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/api/ping', function(req, res) {
    res.send('pong!');
});

app.post('/api/v1/locker-events', function(req, res) {
    const {type, pin, lockerId, timestamp} = req.body;
    console.info(`Přišel požadavek ${type} s daty: ${JSON.stringify({pin, lockerId, timestamp})}`);

    axios.get('https://www.seznam.cz')
        .then(function (response) {
            console.log(`Seznam odpověděl ${response.data.length} znaků.`);
        })

    res.send({
        status: 'OK',
    });
});

app.listen(port);
console.log('Server started at http://localhost:' + port);

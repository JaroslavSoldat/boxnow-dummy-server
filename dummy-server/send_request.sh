#!/bin/sh

curl -X 'POST' \
  'http://localhost:8080/api/v1/locker-events' \
  -H 'accept: */*' \
  -H 'Content-Type: application/json' \
  -d '{
  "type": "pin_entered",
  "pin": "123456",
  "lockerId": "601e7028-0565-0528-0000-052800001239",
  "timestamp": "2021-07-07T11:36:01.698Z"
}'

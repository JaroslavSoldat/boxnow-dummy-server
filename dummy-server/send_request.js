const axios = require('axios')

const url = 'http://localhost:8080/api/v1/locker-events';
const timestamp = (new Date()).toISOString();
const data = {
    type: "pin_entered",
    pin: 123456,
    lockerId: "601e7028-0565-0528-0000-052800001239",
    timestamp,
}

axios.post(url, data)
    .then(function (response) {
        console.log(`Server odpověděl: ${JSON.stringify(response.data)}`);
    })

# Simple dummy REST server for BoxNow project

## Prerequisites
- [node.js](https://nodejs.org/) >= v14 installed

## To prepare
```shell
npm ci
```

## To run server
```shell
npm start
```

## To send testing request
```shell
npm run send_request
```

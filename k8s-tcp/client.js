const net = require('net');
const {version} = require('./package.json');
console.log(`TCP client v${version}`);

const serverIP = process.argv[2] || '127.0.0.1';
const serverPort = process.argv[3] || '1337';

const client = new net.Socket();

console.log(`Connecting to ${serverIP}:${serverPort}`);
client.connect(Number(serverPort), serverIP, () => {
    console.log(`... Connected!`);
    client.write('Hello!');
});

client.on('data', (data) => {
    console.log('\nResponse from server:\n');
    console.log(`${data}`);
    client.destroy();
});

client.on('close', () => {
    console.log('Connection closed');
});

const net = require('net');
const {version} = require('./package.json');
console.log(`TCP server v${version}`);

const id = (Math.random() + 1).toString(36).substring(2, 7);
const host = process.env.HOSTNAME || '? (pls set HOSTNAME)';

const server = net.createServer((socket) => {
    const time = new Date();
    socket.write(`TCP server v${version}\n`);
    socket.write(`${id} @ ${host}\n`);
    socket.write(`${time}\n`);
    socket.end();
});

server.listen(1337, () => console.log('Listening on port 1337'));

process.on('SIGTERM', () => process.exit(0));
